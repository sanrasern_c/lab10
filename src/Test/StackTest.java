package Test;

import static org.junit.Assert.*;

import java.lang.reflect.InvocationTargetException;

import ku.util.Stack;
import ku.util.StackFactory;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class StackTest {
	
	private Stack<String> stack;

	@Before
	public void setUp() throws Exception {
		StackFactory.setStackType( 1 );
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testIsEmptyWhenEmpty() {
		stack = StackFactory.makeStack(0);
		assertTrue( stack.isEmpty() );
	}
	
	@Test
	public void testIsEmptyWhenNull() {
		stack = StackFactory.makeStack(2);
		assertTrue( stack.isEmpty() );
	}
	
	@Test
	public void testEmptyWithNotEmpty() {
		stack = StackFactory.makeStack(2);
		stack.push("Hello");
		assertFalse( stack.isEmpty() );
	}
	
	@Test
	public void testIsFullWithFullStack() {
		stack = StackFactory.makeStack(3);
		stack.push("aaaa");
		stack.push("bbbb");
		stack.push("cccc");
		assertTrue( stack.isFull() );
	}
	
	@Test
	public void testIsFullWithNotFullStack() {
		stack = StackFactory.makeStack(3);
		stack.push("aaaa");
		stack.push("bbbb");
		assertFalse( stack.isFull() );
	}
	
	
	@Test
	public void testIsFullWithOverSize() {
		stack =StackFactory.makeStack(2);
		stack.push("aaaaa");
		stack.push("bbbbb");
		stack.push("ccccc");
		assertTrue( stack.isFull() );	
	}
	
	@Test
	public void testPeekWhenNull() {
		stack = StackFactory.makeStack(1);
		assertNull( stack.peek() );
	}
	
	@Test
	public void testPeekWhenNotNull() {
		stack = StackFactory.makeStack(2);
		stack.push("aaaa");
		stack.push("bbbb");
		assertEquals("bbbb", stack.peek() );
	}
	
	@Test
	public void testPush()  {
		stack = StackFactory.makeStack(3);
		stack.push("aaaa");
		assertEquals( 1 , stack.size() );	
	}
	
	@Test
	public void test(){
	stack = StackFactory.makeStack(2);
	stack.push("aaaaa");
	stack.push("bbbbb");
	stack.push("ccccc");
	stack.push("xxxxx");
	}
	
	
	@Test( expected = IllegalStateException.class)
	public void testPushWhenParamIsNull() {
		stack = StackFactory.makeStack(3);
		stack.push("aaaa");
		stack.push("bbbb");
		stack.push( null );
		fail();
		
	}
	
	@Test( expected = IllegalStateException.class ) 
	public void testPushWhenStackIsFull() {
		stack = StackFactory.makeStack(3);
		stack.push("aaaaa");
		stack.push("bbbbb");
		stack.push("ccccc");
		stack.push("xxxxx");
		fail();
	}
	
	@Test
	public void testSizeWhenStackEmpty() {
		stack = StackFactory.makeStack(2);
		assertEquals( 0 , stack.size() );
	}
	
	@Test
	public void testSizeWhenHaveSize() {
		stack = StackFactory.makeStack(7);
		stack.push("aaaaa");
		stack.push("bbbbb");
		stack.push("ccccc");
		stack.push("xxxxx");
		assertEquals( 4 , stack.size() );
	}
	
	@Test
	public void testPop() {
		stack = StackFactory.makeStack(7);
		stack.push("aaaaa");
		stack.push("bbbbb");
		stack.push("ccccc");
		assertEquals( "ccccc" , stack.pop());
		assertEquals( 2 ,stack.size());
	}
	
	@Test
	public void testCapacity() {
		stack = StackFactory.makeStack(7);
		assertEquals( 7 , stack.capacity() );
	}
	
	
	

}
